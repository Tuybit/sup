//import user model to create a new user
import User from "../models/user.model.js";
//package to encrypt sentative info
import bcryptjs from 'bcryptjs'
import jwt from 'jsonwebtoken'
import { errorHandler } from "../utils/errors.js";


//make async for connection client and server and database
export const signup = async (req, res, next) => {
    //make req to signup
    const { username, email, password, firstName ,lastName } = req.body;

    const hashPassword = bcryptjs.hashSync(password,10)
    //cerate a new user
    const newUser = new User({ username, email, password: hashPassword, firstName ,lastName });

    //if user made mistake and we get error we need to output to user his mistake
    try {
        //save user in database
        await newUser.save()
        res.status(200).json({ msg: "User created successfully" });
    } catch (error){
        next(error);
    }
}

export const signin = async (req, res, next) => {
    const { email, password } = req.body;
    try {
        const validUser = await User.findOne({ email})
        if (!validUser) return next(errorHandler(404, `User doesn't exist`));
        
        const validPassword = bcryptjs.compareSync(password, validUser.password);
        if (!validPassword) return next(errorHandler(401, `Invalid password/email`));
        //grand token to authorized user
        const token = jwt.sign({ id: validUser._id }, process.env.JWT_SECRET)
        //remove sentative info from client json 
        const { password: hashPassword, ...rest } = validUser._doc;
        //after user signin first time and made token we take from his cockies
        const expiryDate = new Date(Date.now() + 3600000);//1hrs
        res.cookie('access_token', token, { httpOnly: true, expires: expiryDate })
            .status(200)
            .json(rest);
    } catch (err) {
        next(err)
    }
}

export const google = async (req, res, next) => {
     try {
    const user = await User.findOne({ email: req.body.email });
    if (user) {
      const token = jwt.sign({ id: user._id }, process.env.JWT_SECRET);
      const { password: hashedPassword, ...rest } = user._doc;
      const expiryDate = new Date(Date.now() + 3600000); // 1 hour
      res
        .cookie('access_token', token, {
          httpOnly: true,
          expires: expiryDate,
        })
        .status(200)
        .json(rest);
    } else {
      const generatedPassword =
        Math.random().toString(36).slice(-8) +
        Math.random().toString(36).slice(-8);
        const hashedPassword = bcryptjs.hashSync(generatedPassword, 10);
        const newUser = new User({
        username:
        req.body.name.split(' ').join('').toLowerCase() +
        Math.random().toString(36).slice(-8),
        email: req.body.email,
        password: hashedPassword,
        firstName: req.body.name.split(' ')[0],
        lastName: req.body.name.split(' ')[1],
        profilePicture: req.body.photo,
        });

        await newUser.save();

        const token = jwt.sign({ id: newUser._id }, process.env.JWT_SECRET);
        const { password: hashPassword2, ...rest } = newUser._doc;
        const expiryDate = new Date(Date.now() + 3600000);//1hrs
        res.cookie('access_token', token, { httpOnly: true, expires: expiryDate })
            .status(200)
            .json(rest)
        }
    } catch (err) {
        next(err)
    }
}

export const signout =  (req, res) => { 
    res.clearCookie('access_token').status(200).json('Sign out success');
}
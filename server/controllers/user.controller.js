import User from '../models/user.model.js';
import { errorHandler } from '../utils/errors.js';
import bcryptjs from 'bcryptjs';

export const getUserList = async (req, res, next) => {
    try {
		const loggedInUserId = req.user._id;
		const filteredUsers = await User.find({ _id: { $ne: loggedInUserId } }).select("-password");

		res.status(200).json(filteredUsers);
	} catch (error) {
		console.error("Error in getUsersForSidebar: ", error.message);
		res.status(500).json({ error: "Internal server error" });
	}
};

export const getFriendList = async (req, res, next) => {
    try {
        const user = await User.findById(req.params.id);
        // Get the list of friend IDs
        const friendIds = user.friends;
        const allFriends = await User.find({ _id: { $in: friendIds } }).select("-password");

        res.status(200).json(allFriends);
    } catch (error) {
        next(error);
    }
};

export const updateUser = async (req, res, next) => {
    if (req.user.id !== req.params.id) 
        return next(errorHandler(401, "You can update only your account!"))
    try {
        if (req.body.password) {
            req.body.password = bcryptjs.hashSync(req.body.password, 10)
        }

        const updatedUser = await User.findByIdAndUpdate(
            req.params.id,
            {
                //update only listed info
                $set: {
                    username: req.body.username,
                    email: req.body.email,
                    password: req.body.password,
                    firstName: req.body.firstName,
                    lastName: req.body.lastName,
                    middleName: req.body.middleName,
                    country: req.body.country,
                    aboutMe: req.body.aboutMe,
                    profilePicture: req.body.profilePicture,
                }
            },
            { new: true } //sends user updated info
        );
        const { password, ...rest } = updatedUser._doc;
        res.status(200).json(rest);
    } catch (error) {
        next(error);
    }
}

export const deleteUser = async (req, res, next) => {
  if (req.user.id !== req.params.id) {
    return next(errorHandler(401, 'You can delete only your account!'));
  }
  try {
    await User.findByIdAndDelete(req.params.id);
    res.status(200).json('User has been deleted...');
  } catch (error) {
    next(error);
  }

}

export const addFriend = async (req, res, next) => {
    if (req.user.id !== req.params.id) 
        return next(errorHandler(401, "You can update only your account!"))
    try {

        if (req.params.id === req.body.friendId)
            return next(errorHandler(409, "Already Exist"))

        const user = await User.findByIdAndUpdate(
            req.params.id,
            { $push: { friends: req.body.friendId } },
            { new: true },
        )
        
        const { password, ...rest } = user._doc;
        res.status(200).json(rest);
    } catch (error) {
        next(error);
    }
}
import express from 'express';
import { getUserList,updateUser, deleteUser,addFriend, getFriendList} from '../controllers/user.controller.js';
import { verifyToken } from '../utils/verifyUser.js';

const router = express.Router();

router.get('/', verifyToken, getUserList); //get users
router.post('/update/:id', verifyToken, updateUser);
router.delete('/delete/:id', verifyToken, deleteUser);

router.get('/friends/:id', verifyToken, getFriendList);
router.post('/add-friend/:id', verifyToken, addFriend);
router.post('/delete-friend/:id', verifyToken, addFriend);

export default router;
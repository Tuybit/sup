import express from 'express';
import mongoose from 'mongoose';
import dotenv from 'dotenv';
import cookieParser from 'cookie-parser';
import path from 'path';

import userRoutes from './routes/user.route.js';
import authRoutes from './routes/auth.route.js';
import messageRoutes from './routes/message.route.js'

import { app, server } from './socket.io/socket.js';

const __dirname = path.resolve();
dotenv.config();



//connect mongoDB
mongoose.connect(process.env.MONGODB)
    
    .then(() => {
        console.log('Connected to MongoDB');
    })
    .catch((err) => {
        console.error(err);
    });

app.use(express.json());
app.use(cookieParser());

app.use(express.static(path.join(__dirname, "/client/dist")));

app.get('/', (req, res) => {
    res.json({ message: "Api works" })
});

app.use("/api/user", userRoutes);
app.use("/api/auth", authRoutes);
app.use("/api/message", messageRoutes);

//deployment


app.get("*", (req, res) => {
    res.sendFile(path.join(__dirname,"client","dist","index.html"))
})

//middleware to handle errors
app.use((err, req, res, nex) => {
    const statusCode = err.statusCode || 500;//if there no status put 500(server error)
    const message = err.message || 'Server Error 8';
    return res.status(statusCode).json({
        success: false,
        message,
        statusCode,
    });
});

server.listen(5000, () => {
    console.log(`server running on http://localhost:5000/`);
});

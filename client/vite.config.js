import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'
import tailwindcss from 'tailwindcss'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  //The proxy option allows you to redirect certain requests to a different server during development
  server: {
    proxy: {
      '/api': {
        target: 'http://localhost:5000',
        secure: false
      }
    }
  }
})

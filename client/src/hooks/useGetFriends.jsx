import React from 'react'
import { useEffect, useState } from 'react'
import { useSelector } from 'react-redux';

function useGetFriends() {
    const [friends, setFriends] = useState([]);
    const { currentUser } = useSelector((state) => state.user);
  
  useEffect(() => {
    const getFriends = async () => {
        try {
            const res = await fetch(`/api/user/friends/${currentUser._id}`);
            const data = await res.json();
            if (data.error) {
                throw new Error(data.error);
            }

            setFriends(data);
        } catch (error) {
        console.log(error);
        }
    }

    getFriends();
  }, [])
  
  return {friends}
}


export default useGetFriends
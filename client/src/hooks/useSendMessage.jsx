import React, { useState } from 'react'
import { setMessages } from '../redux/Chat/useConversation.jsx'
import { useDispatch, useSelector } from 'react-redux';

function useSendMessage() {
  const [loading, setLoading] = useState(false);
  const selectedConversation = useSelector(state => state.conversation.selectedConversation);
  const messages = useSelector(state => state.conversation.messages);
  const dispatch = useDispatch();

  const sendMessage = async (message) => {

  setLoading(true);
    try {
      const res = await fetch(`/api/message/send/${selectedConversation._id}`, {
        method: 'POST',
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ message })
      })
      const data = await res.json();
      if (data.success == false) {
        console.log("error")
        return
      }
      dispatch(setMessages([...messages, data]));
    } catch (error) {
      throw error;
    }finally {
			setLoading(false);
		}
  }
  return {sendMessage, loading}
}

export default useSendMessage
import React from 'react'
import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';

function useGetUsers() {
  const [users, setUsers] = useState([]);
  
  useEffect(() => {
    const getUsers = async () => {
      try {
        const res = await fetch('/api/user/');
        const data = await res.json();
        if (data.error) {
          throw new Error(data.error);
        }

        setUsers(data);
      } catch (error) {
        console.log(error);
      }
    }

    getUsers();
  }, [])
  
  return {users}
}

export default useGetUsers
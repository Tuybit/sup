import React, { useEffect } from 'react'
import { useSocketContext } from '../context/SocketContext';
import { setMessages } from '../redux/Chat/useConversation.jsx';
import { useDispatch, useSelector } from 'react-redux';

function useListenMessages() {
    const { socket } = useSocketContext();
    const messages = useSelector((state) => state.conversation.messages);
    const dispatch = useDispatch();
    
  useEffect(() => {
      //listen for the event
		socket?.on("newMessage", (newMessage) => {
			dispatch(setMessages([...messages, newMessage]));
    });
    
		return () => socket?.off("newMessage");
	}, [socket, dispatch,messages]);
}
export default useListenMessages;
import React, { useEffect,useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { setMessages } from '../redux/Chat/useConversation';

function useGetMessages() {
    const [loading, setLoading] = useState(false);
    const selectedConversation = useSelector(state => state.conversation.selectedConversation);
    const messages = useSelector(state => state.conversation.messages);
    const dispatch = useDispatch();
    useEffect(() => {
        const getMessages = async () => {
            setLoading(true);
            try {
                const res = await fetch(`/api/message/${selectedConversation._id}`)
                const data = await res.json();
                

                dispatch(setMessages(data));
            } catch(error){
                
            } finally {
                setLoading(false)
            }
        }
        if (selectedConversation?._id) getMessages();
    },[selectedConversation?._id, setMessages])
  return {messages, loading}
}

export default useGetMessages
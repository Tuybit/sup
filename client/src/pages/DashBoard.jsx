import React from 'react'
import { useDispatch, useSelector } from 'react-redux';

function DashBoard() {
  const { currentUser } = useSelector((state) => state.user);
  return (
    <div className='max-w-lg mx-auto justify-center'>
      <div className='grid grid-cols-3'>
          <img className='h-24 w-24 rounded-full object-cover self-center' src={currentUser.profilePicture} alt="profile"/>
        <h4 className='self-center'>{currentUser.firstName} {currentUser.lastName}</h4>
        <p className='self-center'> Friends: 15 Invitations: 1</p>
      </div>
      <h2>Friends</h2>
      <div className='grid-cols-3'>
        <aside>
          <ul>
            <li>
              <img src="" alt="profile" />
              <span className="flex-1 ml-3 whitespace-nowrap">Joe goog</span>
            </li>
          </ul>
        </aside>
        <div>
          <h4>Recent Chat</h4>
        </div>
      </div>
    </div>
  )
}

export default DashBoard

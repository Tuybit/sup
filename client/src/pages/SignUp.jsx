import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import OAuth from "../components/OAuth/OAuth.jsx";

function Signup() {
  const [formData, setFormData] = useState({});
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false);
  const nvg = useNavigate();
  const handelChange = (e) => {
    //spread and by id of focus change values
    setFormData({...formData, [e.target.id]: e.target.value})
  }

  const handelSubmit = async (e) => {
    e.preventDefault();

    setLoading(true);
    setError(false);

    const res = await fetch('/api/auth/signup', {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(formData)
    });
    const data = await res.json();
    setLoading(false);
    if (data.success == false) {
      setError(true);
      return
    }
    //if sign up go to sign in
    nvg('/sign-in');
  }
  return (
    <div className='p-3 max-w-lg mx-auto justify-center'>
      <h1 className='text-3xl text-center font-semibold my-7'>Sign Up</h1>
      <form onSubmit={handelSubmit}className='flex flex-col gap-4'>  
        <input type="text" placeholder='Username' id='username' onChange={handelChange} className='input input-bordered '/>
        <input type="email" placeholder='example@email.com' id='email' onChange={handelChange}className='input input-bordered '/>
        <input type="password" placeholder='Password' id='password' onChange={handelChange}className='input input-bordered '/>
        <input type="text" placeholder='First Name' id='firstName' onChange={handelChange}className='input input-bordered '/>
        <input type="text" placeholder='Last Name' id='lastName' onChange={handelChange} className='input input-bordered '/>
        <button disabled={loading} className='btn btn-primary'>{loading ? 'Loading...' : 'Sign Up'}</button>
        <OAuth />
      </form>
      <p className='text-red-700 mt-5'>{error ? error.message || 'Something went wrong!' : ''}</p>
      <h4>Have an account</h4>
      <Link to='/sign-in' className='text-blue-500'>Sign In</Link>
    </div>
  )
}

export default Signup
import React, { useState, useRef, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { getStorage, ref, getDownloadURL, uploadBytesResumable } from 'firebase/storage';
import { app } from '../firebase';
import { updateUserStart, updateUserFailure, updateUserSuccess,deleteUserStart,deleteUserSuccess,deleteUserFailure, signOut } from '../redux/user/userSlice';

function Profile() {
  const { currentUser } = useSelector((state) => state.user);
  const { loading, error } = useSelector((state) => state.user);
   const [updateSuccess, setUpdateSuccess] = useState(false);
  const fileRef = useRef(null);
  const [formData, setFormData] = useState({});
  const [image, setImage] = useState(undefined);
  const [imagePercent, setImagePercent] = useState(0);
  const [imageErr, setImageErr] = useState(false)
  const dispatch = useDispatch();

  //when we chose pic it shows right away
  useEffect(() => {
    if (image) {
      handleFileUpload(image);
    }
  }, [image]);
  
  const handleFileUpload = async (image) => {
    const storage = getStorage(app);
    //need uniqe name to upload 
    const fileName = new Date().getTime() + image.name;
    const storageRef = ref(storage, fileName);
    const uploadTask = uploadBytesResumable(storageRef, image);

    //track every byte we uploading and we can output progress
    uploadTask.on(
      'state_changed',
      (snapshot) => {
        const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        setImagePercent(Math.round(progress));
      },
    (error) => {
      setImageErr(true)
    },
    () => {
      getDownloadURL(uploadTask.snapshot.ref).then((downloadURL) => {
        setFormData({ ...formData, profilePicture: downloadURL })
      }
        );
    });
  }

  const handleChange = (e) => {
    setFormData({...formData, [e.target.id]: e.target.value})
  }

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      dispatch(updateUserStart());
      const res = await fetch(`/api/user/update/${currentUser._id}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(formData),
      });
      const data = await res.json();
      if (data.success === false) {
        dispatch(updateUserFailure(data));
        return;
      }
      dispatch(updateUserSuccess(data));
      setUpdateSuccess(true);
    } catch (error) {
      dispatch(updateUserFailure(error));
    }
  }

  const handleDelete = async () => {
    try {
      dispatch(deleteUserStart());
      const res = await fetch(`/api/user/delete/${currentUser._id}`, {
        method: 'DELETE',
      });
      const data = await res.json();
      console.log(res)
      if (data.success === false) {
        dispatch(deleteUserFailure(data));
        return;
      }
      dispatch(deleteUserSuccess(data));
    } catch (error) {
      dispatch(deleteUserFailure(error));
    }
  };

  const handleSignOut = async () => {
    try {
      await fetch('/api/auth/signout');
      dispatch(signOut());
    } catch (error) {
      console.log(error);
    }
  }
  return (
    <div className='p-3 max-w-lg mx-auto justify-center'>
      <h1 className='text-3xl text-center font-semibold my-7'>Profile</h1>
      <form className='flex flex-col gap-4' onSubmit={handleSubmit}>
        {/* upload only pics */}
        <input type="file" ref={fileRef} hidden accept='image/*' onChange={(e) => setImage(e.target.files[0])}/>
        {/* firebase storage rule:
        allow read,
        write: if 
        request.resource.size < 2 *1024 *1024 &&
        request.resource.contentType.matches("image/.*") */}
        <img className='h-24 w-24 self-center cursor-pointer rounded-full object-cover mt-2' src={formData.profilePicture || currentUser.profilePicture} alt="profile" onClick={() => fileRef.current.click()} />
        <p>{imageErr ? (<span className="text-red-700">Error uploading image</span>) : imagePercent > 0 && imagePercent < 100 ? (<span className='text-slate-700'> {`Uploading image... ${imagePercent}`}</span>) : imagePercent === 100 ? (<span className='text-green-700'> Image uploaded</span>) : ''}</p>
        <input defaultValue={currentUser.username} type="text" id='username' placeholder='Username'onChange={handleChange}className='input input-bordered '/>
        <input defaultValue={currentUser.email} type="email" id='email' placeholder='Email'onChange={handleChange}className='input input-bordered '/>
        <input type="password" id='password' placeholder='Password' className='input input-bordered '/>
        <input defaultValue={currentUser.firstName}type="text" id='firstName' placeholder='First name'onChange={handleChange} className='input input-bordered '/>
        <input defaultValue={currentUser.lastName}type="text" id='lastName' placeholder='Last name'onChange={handleChange} className='input input-bordered '/>
        <input defaultValue={currentUser.middleName}type="text" id='middletName' placeholder='Middle name'onChange={handleChange} className='input input-bordered '/>
        <input defaultValue={currentUser.country} type="text" id='country' placeholder='Country'onChange={handleChange} className='input input-bordered '/>
        <input defaultValue={currentUser.aboutMe} type="text" id='aboutMe' placeholder='About me' onChange={handleChange} className='input input-bordered '/>
        <button className='btn btn-active btn-accent'>{loading ? 'Loading...' : 'Update'}</button>
      </form>
      <div className='flex justify-between mt-5'>
        <span className='text-red-700 cursor-pointer' onClick={handleDelete}>Delete Account</span>
        <span onClick={handleSignOut} className='text-red-700 cursor-pointer' >Sign Out</span>
      </div>
      <p className='text-green-700 mt-5'>
        {updateSuccess && 'User is updated successfully!'}
      </p>
    </div>
  )
}

export default Profile
import React from 'react'
import { Link } from 'react-router-dom'
function Homepage() {
  return (
    <main className='px-4 py-12 max-w-2xl mx-auto'>
      <h1 className='text-3xl font-bold  mb-4 text-red-200'>SUP APP</h1>
      <p className='mb-4 text-slate-700'>Introducing "SUP" - a revolutionary new chat app that allows you to have visual conversations with your friends,
family, or colleagues without the need for appointments or agreements. With "SUP", you can easily send short "sup"
videos to your desired recipient and engage in a fun and interactive way.</p>
      <Link to='/sign-in' id='logo' className='btn'>
        <h1>Try the app</h1>
      </Link>
    </main>
  );
}

export default Homepage
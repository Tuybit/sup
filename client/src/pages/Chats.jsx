import Conversations from '../components/Conversations'
import MessageContainer from '../components/MessageContainer.jsx'
import useGetConversations from '../hooks/useGetUsers'
import React, { useState } from 'react';
import { CgMoreVerticalR } from "react-icons/cg";

function Chats() {
  
  const { loading, conversations } = useGetConversations();
  console.log(conversations);
  const [isSidebarOpen, setIsSidebarOpen] = useState(true);
   const toggleSidebar = () => {
    setIsSidebarOpen(!isSidebarOpen);
  };

  return (
    <div className='flex justify-center'>
      {/* sidebar */}
      <CgMoreVerticalR onClick={toggleSidebar} />
      <div className={`w-64 bg-gray-200 ${isSidebarOpen ? '' : 'hidden'}`}>
        <div className='p-4'>
          <Conversations />
        </div>
      </div>
      {/* messages */}
      <div className='flex-1'>
        <div className='max-w-screen-lg mx-auto'>
          <MessageContainer/>
        </div>
      </div>
    </div>
  )
}

export default Chats

{/* <div className='flex justify-center'>
      <div className=''>
        <Conversations/>
      </div>
      <div className=''>
        <MessageContainer/>
      </div>
    </div > */}
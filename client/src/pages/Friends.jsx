import React from 'react'
import User from '../components/User'
import { useDispatch, useSelector } from 'react-redux';
import useGetUsers from '../hooks/useGetUsers.jsx';
import useGetFriends from '../hooks/useGetFriends.jsx';

function Friends() {
  const { currentUser } = useSelector((state) => state.user);
  const { users } = useGetUsers();
  const { friends } = useGetFriends();

  // Create an array of friend IDs
  const friendIds = friends.map((friend) => friend._id);

  // Filter out users who are already friends
  const nonFriends = users.filter((user) => !friendIds.includes(user._id));
  
  return (
    <div className='p-3 max-w-lg mx-auto justify-center'>
      <h2>Friends</h2>
      <div> 
        {friends.map((user) => (
          <User key={user._id} user={ user }/>
        ))}
      </div>

      <h3>People You May Like</h3>
      <div> 
        {nonFriends.map((user) => (
          <User key={user._id} user={user} />
        ))}
      </div>
  </div>
  )
}

export default Friends
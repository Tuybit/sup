import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { signInStart, signInSuccess, signInFailure } from '../redux/user/userSlice.jsx';
import { useDispatch, useSelector } from 'react-redux';
import OAuth from "../components/OAuth/OAuth.jsx";

function Signin() {
  const [formData, setFormData] = useState({});
  const { loading, error } = useSelector((state) => state.user);
  const nvg = useNavigate();
  const dispatch = useDispatch();

  const handelChange = (e) => {
    //spread and by id of focus change values
    setFormData({...formData, [e.target.id]: e.target.value})
  }

  const handelSubmit = async (e) => {
    e.preventDefault();

    try {
      dispatch(signInStart());

      const res = await fetch('/api/auth/signin', {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(formData)
      });
      
      const data = await res.json();
      if (data.success == false) {
        dispatch(signInFailure(data));
        return
      }

      dispatch(signInSuccess(data));
      //if sign in go to dashboard
      nvg('/dashboard');
    } catch (error) {
      dispatch(signInFailure(error))
    }
  }

  return (
    <div className='p-3 max-w-lg mx-auto justify-center'>
      <h1 className='text-3xl text-center font-semibold my-7'>Sign In</h1>
      <form onSubmit={handelSubmit} className='flex flex-col gap-4'> 
        <input type="email" placeholder='example@email.com' id='email' onChange={handelChange} className='input input-bordered '/>
        <input type="password" placeholder='Password' id='password' onChange={handelChange} className='input input-bordered'/>
        <button disabled={loading} className='btn btn-primary'>{loading ? 'Loading...' : 'Sign In'}</button>
        <OAuth/>
      </form>
      <p className='text-red-700 mt-5'>{error ? error.message || 'Something went wrong!' : ''}</p>
      <h4>Dont have an account?</h4>
      <Link to='/sign-up' className='text-blue-500'>Sign Up</Link>
    </div>
  )
}

export default Signin
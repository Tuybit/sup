// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: import.meta.env.VITE_FIREBASE_KEY ,
  authDomain: "sup-app-396c4.firebaseapp.com",
  projectId: "sup-app-396c4",
  storageBucket: "sup-app-396c4.appspot.com",
  messagingSenderId: "437399296703",
  appId: "1:437399296703:web:37117fed30ca6e5c07440a"
};

// Initialize Firebase
export const app = initializeApp(firebaseConfig);
//show OnlineUsers in app live
import { createContext, useState, useEffect, useContext } from 'react';
import {useSelector } from 'react-redux';
import io from 'socket.io-client';

const SocketContext = createContext();


export const useSocketContext = () => {
    return useContext(SocketContext);
};

export const SocketContextProvider = ({ children }) => {
    const { currentUser } = useSelector((state) => state.user);
    const [socket, setSocket] = useState(null);
    const [onlineUsers, setOnlineUsers] = useState([]);

    useEffect(() => {
        if (currentUser) {
            //connect to the server
            //change the url to your server
            const socket = io('https://sup-chat.onrender.com', {
                query: {
                    userId: currentUser._id,
                },
            });
            //listen for the event
            setSocket(socket);
            //socket.on is used to listan to the events. can be used on client and server sides
            socket.on('getOnlineUsers', (users) => {
                setOnlineUsers(users);
            })
            //disconnect
            return () => socket.close();
        } else {
            if (socket) {
                socket.close();
                setSocket(null);
            }
        }
    }, [currentUser])
    
    return <SocketContext.Provider value={{socket,onlineUsers}}>{children}</SocketContext.Provider>;
}
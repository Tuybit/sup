import { createSlice } from "@reduxjs/toolkit";

//The initial state value for this slice of state.
const initialState = {
    selectedConversation: null,
    messages:[],
    loading: false,
};

const conversationSlice  = createSlice({
    name: 'conversation',
    initialState,
    //change initial state value
    reducers: {
        setSelectedConversation: (state, action) => {
            state.selectedConversation = action.payload;
        },
        pushMessage: (state, action) => {
            state.messages.push(action.payload);
        },
        setMessages: (state, action) => {
            state.messages = action.payload;
        },
    },
});

export const { setSelectedConversation, setMessages,pushMessage } = conversationSlice.actions;

export default conversationSlice.reducer;
import { createSlice } from "@reduxjs/toolkit";

//The initial state value for this slice of state.
const initialState = {
    currentUser: null,
    loading: false,
    error: false,
};

const userSlice = createSlice({
    name: 'user',
    initialState,
    //change initial state value
    reducers: {
        signInStart: (state) => {
            state.loading = true;
        },
        signInSuccess: (state, action) => {
            //if signin return user info
            state.currentUser = action.payload;
            state.loading = false;
            state.error = false;
        },
        signInFailure: (state, action) => {
            state.loading = false;
            state.error = action.payload;
        },
        updateUserStart: (state) => {
            state.loading = false;
        },
        updateUserFailure: (state,action) => {
            state.loading = false;
            state.error = action.payload;
        },
        updateUserSuccess: (state,action) => {
            state.currentUser = action.payload;
            state.loading = false;
            state.error = false;
        },
        deleteUserStart: (state) => {
            state.loading = true;
        },
        deleteUserSuccess: (state) => {
            state.currentUser = null;
            state.loading = false;
            state.error = false;
        },
        deleteUserFailure: (state,action) => {
            state.loading = false;
            state.error = action.payload;
        },
        signOut: (state) => {
            state.currentUser = null;
            state.loading = false;
            state.error = false;
        }
    }
});

export const { signInStart, signInSuccess, signInFailure, updateUserStart, updateUserFailure,updateUserSuccess,deleteUserStart,deleteUserSuccess,deleteUserFailure, signOut } = userSlice.actions;

export default userSlice.reducer;
import { combineReducers, configureStore, } from '@reduxjs/toolkit';
import userReducer from './user/userSlice.jsx';
import { persistReducer, persistStore } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import useConversation from './Chat/useConversation.jsx';


const rootReducer = combineReducers({user: userReducer, conversation: useConversation});

//config to save in local storage
const persistConfig = {
    key: 'root', //name where we gonna save inside local storage
    version: 1,
    storage, //saves in local storage
}

const persistedReducer = persistReducer(persistConfig, rootReducer);
export const store = configureStore({
    reducer: persistedReducer,
    middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware({
        serializableCheck: false,
        }),
});

export const persistor = persistStore(store);
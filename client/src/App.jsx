import { BrowserRouter, Routes, Route } from 'react-router-dom';

import Header from './components/header/Header';
import Footer from './components/footer/Footer';
import PrivateRoute from './components/PrivateRoute';
import Home from './pages/HomePage';
import SignIn from './pages/SignIn';
import SignUp from './pages/SignUp';
import DashBoard from './pages/DashBoard';
import Profile from './pages/Profile';
import Chats from './pages/Chats';
import Friends from './pages/Friends';
function App() {
  return (
    <BrowserRouter >
      <Header/>
      <Routes>
        <Route index path="/" element={<Home />} />
        <Route path="/sign-in" element={<SignIn />} />
        <Route path="/sign-up" element={<SignUp />} />

        {/* make the page private for a user */}
        <Route element={<PrivateRoute/>}>
          <Route path="/profile" element={<Profile />} />
          <Route path="/dashboard" element={<DashBoard />} />
          <Route path="/chats" element={<Chats />} />
          <Route path="/friends" element={<Friends />} />

        </Route>
      </Routes>
      {/* <Footer/> */}
    </BrowserRouter>
  );
}

export default App
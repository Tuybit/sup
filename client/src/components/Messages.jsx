import { useEffect, useRef } from 'react'
import MessageSkeleton from './MessageSkeleton'
import Message from './Message'
import useGetMessages from '../hooks/useGetMessages'
import useListenMessages from '../hooks/useListenMessages';

function Messages() {
	const { messages, loading } = useGetMessages();
	useListenMessages();
	const lastMsgRef = useRef();

	// Scroll to the bottom of the messages container
	useEffect(() => {
		setTimeout(() => {
			lastMsgRef.current?.scrollIntoView({ behavior: "smooth" });
		}, 100);
	}, [messages]);

  return (
	  <div className='px-4 flex-1 overflow-auto'>
		
		{/* if there are messages show them */}
		{!loading && messages.length > 0 && messages.map((message) => (
			<div key={message._id} ref={lastMsgRef}>
				<Message message={message} />
			</div>
		))}

		{/* while loading show skeleton */}
		{loading && [...Array(3)].map((_, idx) => <MessageSkeleton key={idx} />)}
		  
		{/* if no messages show this */}
		{!loading && messages.length === 0 && (
			<p className='text-center'>Send a message to start the conversation</p>
		)}
	</div>
  )
}

export default Messages
import React from 'react'
import { useSelector } from 'react-redux';
import { Outlet,Navigate } from 'react-router-dom';

//makes page private
function PrivateRoute() {
    //check state if we have user
    const { currentUser } = useSelector(state => state.user)
    // if user sign in  go to child, if not go to sign in page
    return currentUser ? <Outlet/> : <Navigate to='/sign-in'/>
}

export default PrivateRoute
import React from 'react';
import { Link } from 'react-router-dom';
import {useSelector } from 'react-redux';

function Header() {
    const {currentUser} = useSelector(state => state.user)
    return (
        <header className='flex justify-between items-center max-w-6xl mx-auto p-3'>
            <Link to='/' id='logo'>
                <h1 className='font-bold'>SUP</h1>
            </Link>

            <div className="flex-none">
                <ul className="menu menu-horizontal px-1">
                <li><Link to= '/'>About</Link></li>
                    {currentUser ? (
                    <li>
                    <details>
                        <summary>
                            Menu
                        </summary>
                        <ul className="p-2 bg-base-100 rounded-t-none absolute z-10">
                            <li><Link to='/dashboard'>Dashboard</Link></li>
                            <li><Link to='/profile'>Profile</Link></li>
                            <li><Link to='/friends'>Friends</Link></li>
                            <li><Link to= '/chats'>Chats</Link></li>
                        </ul>
                    </details>
                    </li>
                    ): (<li><Link to= '/sign-in'>Sign In</Link></li>)}
                </ul>
            </div>
            {/* <ul className='flex gap-4'>
                <Link to='/'>
                    <li>Home</li>
                </Link>
                <Link to='/about'>
                    <li>About</li>
                </Link>
                <Link to='/profile'>
                    {currentUser ? (
                    <img src={currentUser.profilePicture} alt='profile' className='h-7 w-7 rounded-full object-cover' />
                    ) : (
                    <li>Sign In</li>
                    )}
                </Link>
            </ul> */}
        </header>
    );
}

export default Header
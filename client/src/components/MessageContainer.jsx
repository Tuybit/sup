import React, { useEffect } from 'react'
import Messages from '../components/Messages'
import MessageInput from '../components/MessageInput'
import { useDispatch, useSelector } from 'react-redux';
import { setSelectedConversation } from '../redux/Chat/useConversation.jsx'


function MessageContainer() {
  const selectedConversation = useSelector(state => state.conversation.selectedConversation);

  const dispatch = useDispatch();

  useEffect(() => {
    //clean up the selectedConversation when the component unmounts
    return () => dispatch(setSelectedConversation(null));
  }, [])
  
  return (
    <div className='border-l-[3px] border-sky-500 h-screen md:min-w-[650px] flex flex-col'>
      {!selectedConversation ? (<NoChatSelected />) : (
        <>
          <Messages />
          <MessageInput/>
        </>
        )}
    </div>
  )
}

const NoChatSelected = () => {
	return (
		<div className='flex items-center justify-center w-full h-full'>
			<div className='px-4 text-center sm:text-lg md:text-xl font-semibold flex flex-col items-center gap-2'>
        <p>Welcome</p>
				<p>Select a chat to start messaging</p>
			</div>
		</div>
	);
};

export default MessageContainer
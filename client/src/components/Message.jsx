import React from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { extractTime } from '../../utils/extractTime';
function Message(message) {
  const { currentUser } = useSelector((state) => state.user);
  const selectedConversation = useSelector(state => state.conversation.selectedConversation);

  //style
  const fromMe = message.message.senderId === currentUser._id;
  const chatClassName = fromMe ? 'chat-end' : 'chat-start';
  const profilePic = fromMe ? currentUser.profilePicture : selectedConversation.profilePicture; 
  const bubbleBgColor = fromMe ? 'chat-bubble-success' : '';
  const fullName = fromMe ? `${currentUser.firstName} ${currentUser.lastName}` : `${selectedConversation.firstName} ${selectedConversation.lastName}`;
  const formattedDate = extractTime(message.message.createdAt);
  
  return (
    <div className={`chat ${chatClassName}`}>
        <div className="chat-image avatar">
            <div className="w-10 rounded-full">
            <img alt="Tailwind CSS chat bubble component" src={profilePic} />
            </div>
        </div>
        <div className="chat-header">
            {fullName}
          <time className="text-xs opacity-50">{formattedDate}</time>
            </div >
      <div className={`chat-bubble ${bubbleBgColor}`}>{message.message.message}</div>
    </div>
  )
}

export default Message
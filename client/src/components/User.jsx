import React, { useEffect } from 'react'
import { FiUserCheck } from "react-icons/fi";
import { FiX } from "react-icons/fi";
import { useDispatch, useSelector } from 'react-redux';

function User(user) {
  const { currentUser } = useSelector((state) => state.user);
  const friendId = user.user._id;

  const handleAddFreind = async () => {
    try {
      console.log("start")
      const res = await fetch(`/api/user/add-friend/${currentUser._id}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({friendId}),
      });
      const data = await res.json();
      console.log(data)
      if (data.success === false) {
        return;
      }
    } catch (error) {
      console.log(error);
    }
  }

  return (
      <div className='grid grid-cols-2 gap-x-4 p-3 max-w-[250px] bg-teal-300 glass rounded'>
        <div className=' row-span-2'>
            <div className='avatar online row-span-2'>
            <div className="w-24 rounded-full">
            <img src={user.user.profilePicture} alt='avatar' />
            </div>
        </div>
        </div>
      <h4 className=' self-center'>{user.user.firstName} { user.user.lastName}</h4>
        <div className='flex gap-2'>
              <FiUserCheck className='cursor-pointer' onClick={handleAddFreind}/>
              <FiX className='cursor-pointer'/>
        </div>
    </div>
  )
}

export default User

import React from 'react'
import { useDispatch, useSelector } from 'react-redux';
import { setSelectedConversation } from '../redux/Chat/useConversation.jsx'
import { useSocketContext } from '../context/SocketContext.jsx';

function Conversation({ conversation }) {
  const selectedConversation = useSelector(state => state.conversation.selectedConversation);
  const isSelected = selectedConversation?._id === conversation._id;
  const dispatch = useDispatch();
  const { onlineUsers } = useSocketContext();
  const isOnline = onlineUsers.includes(conversation._id);

  const handleSelect = () => {
    dispatch(setSelectedConversation(conversation));
  };
  return (
    <div className={`flex gap-2 items-center hover:bg-slate-600 rounded p-2 py-1 cursor-pointer ${isSelected ? "bg-sky-500" : ""}`} onClick={handleSelect}>
        <div className={`avatar ${isOnline ? "online" : ""}`}>
            <div className="xs:w-10 md:w-24 rounded-full">
          <img src={ conversation.profilePicture} alt='avatar' />
          <img src={ conversation.profilePicture} alt='avatar' />
            </div>
        </div>

        <div className='flex flex-col flex-1'>
            <div>
            <p className='font-bold text-grey- 900'>{ conversation.firstName} { conversation.lastName}</p>
            </div>
        </div>

        <div className='divider my-0 py-0 h-1'/>
    </div>
  )
}

export default Conversation
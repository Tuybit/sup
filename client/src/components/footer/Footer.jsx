import React from 'react'
import { Link } from 'react-router-dom';
function Footer() {
    return (
        <footer className="footer grid-rows-2 p-10 bg-neutral text-neutral-content flex justify-around fxed bottom-0">
            <nav>
                <h6 className="footer-title">Services</h6> 
                <a className="link link-hover">Branding</a>
            </nav> 
            <nav>
                <h6 className="footer-title">Company</h6> 
                <a className="link link-hover">About us</a>
            </nav> 
            <nav>
                <h6 className="footer-title">Legal</h6> 
                <a className="link link-hover">Terms of use</a>
            </nav> 
            <nav>
                <h6 className="footer-title">Social</h6> 
                <a className="link link-hover">Twitter</a>
            </nav> 
        </footer>
    );
}

export default Footer